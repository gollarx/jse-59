package ru.t1.shipilov.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.shipilov.tm.api.endpoint.ISystemEndpoint;
import ru.t1.shipilov.tm.api.service.ICommandService;
import ru.t1.shipilov.tm.api.service.IPropertyService;
import ru.t1.shipilov.tm.listener.AbstractListener;

@Component
public abstract class AbstractSystemListener extends AbstractListener {

    @NotNull
    @Autowired
    protected ICommandService commandService;

    @NotNull
    @Autowired
    protected IPropertyService propertyService;

    @NotNull
    @Autowired
    protected ISystemEndpoint systemEndpoint;

}
