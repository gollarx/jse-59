package ru.t1.shipilov.tm.repository.model;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.shipilov.tm.api.repository.model.IRepository;
import ru.t1.shipilov.tm.enumerated.Sort;
import ru.t1.shipilov.tm.model.AbstractModel;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
@Scope("prototype")
@NoArgsConstructor
@AllArgsConstructor
public abstract class AbstractRepository<M extends AbstractModel> implements IRepository<M> {

    @NotNull
    @PersistenceContext
    protected EntityManager entityManager;

    protected abstract Class<M> getEntityClass();

    @Override
    public void add(@NotNull final M model) {
        entityManager.persist(model);
    }

    @Override
    public void clear() {
        for (final M model : findAll())
            entityManager.remove(model);
    }

    @Override
    public boolean existsById(@NotNull final String id) {
        return findOneById(id) != null;
    }

    @Override
    @NotNull
    public List<M> findAll() {
        @NotNull String query = "SELECT m FROM " + getEntityClass().getSimpleName() + " m";
        return entityManager.createQuery(query, getEntityClass()).getResultList();
    }

    @Override
    @NotNull
    public List<M> findAll(@Nullable final Sort sort) {
        @NotNull String query = "SELECT m FROM " + getEntityClass().getSimpleName() + " m";
        @NotNull String orderStatement = " ORDER BY m." + (sort != null ? sort.getSortField() : Sort.BY_CREATED.getSortField());
        query += orderStatement;
        return entityManager.createQuery(query, getEntityClass()).getResultList();
    }

    @Override
    @Nullable
    public M findOneById(@NotNull final String id) {
        @NotNull String query = "SELECT m FROM " + getEntityClass().getSimpleName() + " m WHERE m.id = :id";
        return entityManager.createQuery(query, getEntityClass())
                .setParameter("id", id)
                .getResultList().stream()
                .findFirst()
                .orElse(null);
    }

    @Override
    @Nullable
    public M findOneByIndex(@NotNull final Integer index) {
        @NotNull String query = "SELECT m FROM " + getEntityClass().getSimpleName() + " m";
        return entityManager.createQuery(query, getEntityClass())
                .setMaxResults(index)
                .getResultList().stream()
                .skip(index - 1)
                .findFirst()
                .orElse(null);
    }

    @Override
    public int getSize() {
        @NotNull String query = "SELECT COUNT(m) FROM " + getEntityClass().getSimpleName() + " m";
        return entityManager.createQuery(query, Long.class).getSingleResult().intValue();
    }

    @Override
    public void remove(@NotNull final M model) {
        entityManager.remove(model);
    }

    @Override
    public void removeById(@NotNull final String id) {
        entityManager.remove(findOneById(id));
    }

    @Override
    public void update(@NotNull M model) {
        entityManager.merge(model);
    }

}
