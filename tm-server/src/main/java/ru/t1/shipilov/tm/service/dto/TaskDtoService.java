package ru.t1.shipilov.tm.service.dto;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.shipilov.tm.api.repository.dto.ITaskDtoRepository;
import ru.t1.shipilov.tm.api.service.dto.ITaskDtoService;
import ru.t1.shipilov.tm.enumerated.Sort;
import ru.t1.shipilov.tm.enumerated.Status;
import ru.t1.shipilov.tm.exception.entity.TaskNotFoundException;
import ru.t1.shipilov.tm.exception.entity.field.*;
import ru.t1.shipilov.tm.dto.model.TaskDTO;

import java.util.Collections;
import java.util.List;

@Service
@NoArgsConstructor
@AllArgsConstructor
public final class TaskDtoService implements ITaskDtoService {

    @NotNull
    @Autowired
    private ApplicationContext context;

    @NotNull
    @Autowired
    private ITaskDtoRepository repository;

    @Override
    @SneakyThrows
    @Transactional
    public void clear(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        repository.clear(userId);
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public List<TaskDTO> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull List<TaskDTO> result;
        result = repository.findAll(userId);
        return result;
    }

    @Override
    @SneakyThrows
    @Transactional
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        boolean result;
        result = repository.existsById(userId, id);
        return result;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public TaskDTO findOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable TaskDTO result;
        result = repository.findOneById(userId, id);
        return result;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public TaskDTO findOneByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 1) throw new IndexIncorrectException();
        @Nullable TaskDTO result;
        result = repository.findOneByIndex(userId, index);
        return result;
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        repository.removeById(userId, id);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 1) throw new IndexIncorrectException();
        @Nullable TaskDTO task;
        task = repository.findOneByIndex(userId, index);
        if (task == null) throw new TaskNotFoundException();
        repository.remove(userId, task);
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    @SuppressWarnings({"unchecked"})
    public List<TaskDTO> findAll(@Nullable final String userId, @Nullable final Sort sort) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull List<TaskDTO> result;
        result = repository.findAll(userId, sort);
        return result;
    }

    @Override
    @SneakyThrows
    @Transactional
    public void changeTaskStatusById(@Nullable final String userId, @Nullable final String id, @Nullable final Status status) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final TaskDTO task;
        task = repository.findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        if (status == null) throw new StatusIncorrectException();
        task.setStatus(status);
        repository.update(task);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void changeTaskStatusByIndex(@Nullable final String userId, @Nullable final Integer index, @Nullable final Status status) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 1) throw new IndexIncorrectException();
        @Nullable final TaskDTO task;
        task = repository.findOneByIndex(userId, index);
        if (task == null) throw new TaskNotFoundException();
        if (status == null) throw new StatusIncorrectException();
        task.setStatus(status);
        repository.update(task);
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public List<TaskDTO> findAllByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        @Nullable final List<TaskDTO> result;
        result = repository.findAllByProjectId(userId, projectId);
        return result;
    }

    @Override
    @SneakyThrows
    @Transactional
    public void create(@Nullable final String userId, @Nullable final String name, @Nullable final String description) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @Nullable TaskDTO task = new TaskDTO();
        task.setName(name);
        if (description != null && !description.isEmpty())
            task.setDescription(description);
        repository.add(userId, task);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void add(@Nullable final String userId, @Nullable final TaskDTO task) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (task == null) throw new TaskNotFoundException();
        repository.add(userId, task);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void updateById(@Nullable final String userId, @Nullable final String id, @Nullable final String name, @Nullable final String description) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @Nullable TaskDTO task;
        task = repository.findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        if (description != null && !description.isEmpty())
            task.setDescription(description);
        repository.update(task);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void updateByIndex(@Nullable final String userId, @Nullable final Integer index, @Nullable final String name, @Nullable final String description) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 1) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @Nullable TaskDTO task;
        task = repository.findOneByIndex(userId, index);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        if (description != null && !description.isEmpty())
            task.setDescription(description);
        repository.update(task);
    }

}
