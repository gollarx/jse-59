package ru.t1.shipilov.tm.service.dto;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.shipilov.tm.api.repository.dto.IProjectDtoRepository;
import ru.t1.shipilov.tm.api.repository.dto.ITaskDtoRepository;
import ru.t1.shipilov.tm.api.service.dto.IProjectTaskDtoService;
import ru.t1.shipilov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.shipilov.tm.exception.entity.TaskNotFoundException;
import ru.t1.shipilov.tm.exception.entity.field.IndexIncorrectException;
import ru.t1.shipilov.tm.exception.entity.field.ProjectIdEmptyException;
import ru.t1.shipilov.tm.exception.entity.field.TaskIdEmptyException;
import ru.t1.shipilov.tm.exception.entity.field.UserIdEmptyException;
import ru.t1.shipilov.tm.dto.model.ProjectDTO;
import ru.t1.shipilov.tm.dto.model.TaskDTO;

import java.util.List;

@Service
@NoArgsConstructor
@AllArgsConstructor
public final class ProjectTaskDtoService implements IProjectTaskDtoService {

    @NotNull
    @Autowired
    private IProjectDtoRepository projectRepository;

    @NotNull
    @Autowired
    private ITaskDtoRepository taskRepository;

    @Override
    @SneakyThrows
    @Transactional
    public void bindTaskToProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        @Nullable final TaskDTO task;
        if (!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();
        task = taskRepository.findOneById(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(projectId);
        taskRepository.update(task);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeProjectById(@Nullable final String userId, @Nullable final String projectId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        @NotNull final List<TaskDTO> tasks;
        if (!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();
        if (!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();
        tasks = taskRepository.findAllByProjectId(userId, projectId);
        for (final TaskDTO task : tasks) taskRepository.removeById(userId, task.getId());
        projectRepository.removeById(userId, projectId);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeProjectByIndex(@Nullable final String userId, @Nullable final Integer projectIndex) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectIndex == null || projectIndex < 1) throw new IndexIncorrectException();
        @NotNull final List<TaskDTO> tasks;
        @Nullable ProjectDTO project;
        project = projectRepository.findOneByIndex(userId, projectIndex);
        if (project == null) throw new ProjectNotFoundException();
        tasks = taskRepository.findAllByProjectId(userId, project.getId());
        for (final TaskDTO task : tasks) taskRepository.removeById(userId, task.getId());
        projectRepository.removeById(userId, project.getId());
    }

    @Override
    @SneakyThrows
    @Transactional
    public void unbindTaskFromProject(@Nullable final String userId,
                                      @Nullable final String projectId,
                                      @Nullable final String taskId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        @Nullable final TaskDTO task;
        if (!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();
        task = taskRepository.findOneById(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(null);
        taskRepository.update(task);
    }

}
