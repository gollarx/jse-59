package ru.t1.shipilov.tm.service.dto;

import liquibase.Liquibase;
import liquibase.exception.LiquibaseException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.t1.shipilov.tm.api.repository.dto.IProjectDtoRepository;
import ru.t1.shipilov.tm.api.repository.dto.ITaskDtoRepository;
import ru.t1.shipilov.tm.api.service.dto.IProjectDtoService;
import ru.t1.shipilov.tm.api.service.dto.IProjectTaskDtoService;
import ru.t1.shipilov.tm.api.service.dto.ITaskDtoService;
import ru.t1.shipilov.tm.api.service.dto.IUserDtoService;
import ru.t1.shipilov.tm.configuration.ServerConfiguration;
import ru.t1.shipilov.tm.dto.model.ProjectDTO;
import ru.t1.shipilov.tm.dto.model.TaskDTO;
import ru.t1.shipilov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.shipilov.tm.exception.entity.TaskNotFoundException;
import ru.t1.shipilov.tm.exception.entity.field.ProjectIdEmptyException;
import ru.t1.shipilov.tm.exception.entity.field.TaskIdEmptyException;
import ru.t1.shipilov.tm.exception.entity.field.UserIdEmptyException;
import ru.t1.shipilov.tm.marker.UnitCategory;
import ru.t1.shipilov.tm.migration.AbstractSchemeTest;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import static ru.t1.shipilov.tm.constant.ProjectTaskConstant.*;

@Category(UnitCategory.class)
public class ProjectTaskDtoServiceTest extends AbstractSchemeTest {

    @NotNull
    private ApplicationContext context = new AnnotationConfigApplicationContext(ServerConfiguration.class);

    @NotNull
    private IProjectDtoRepository projectRepository = context.getBean(IProjectDtoRepository.class);

    @NotNull
    private ITaskDtoRepository taskRepository = context.getBean(ITaskDtoRepository.class);

    @NotNull
    private IProjectTaskDtoService projectTaskService = context.getBean(IProjectTaskDtoService.class);

    @NotNull
    private IUserDtoService userService = context.getBean(IUserDtoService.class);

    @NotNull
    private IProjectDtoService projectDtoService = context.getBean(IProjectDtoService.class);

    @NotNull
    private ITaskDtoService taskDtoService = context.getBean(ITaskDtoService.class);

    @Nullable
    private static String USER_ID_1;

    @Nullable
    private static String USER_ID_2;

    private static long USER_ID_COUNTER = 0;

    @NotNull
    private List<ProjectDTO> projectList;

    @NotNull
    private List<TaskDTO> taskList;

    @BeforeClass
    public static void changeSchema() throws LiquibaseException {
        final Liquibase liquibase = liquibase("changelog/changelog-master.xml");
        liquibase.dropAll();
        liquibase.update("scheme");
    }

    @Before
    public void init() {
        USER_ID_COUNTER++;
        USER_ID_1 = userService.create("proj_tsk_serv_mod_usr_1_" + USER_ID_COUNTER, "1").getId();
        USER_ID_2 = userService.create("proj_tsk_serv_mod_usr_2_" + USER_ID_COUNTER, "1").getId();
        projectList = new ArrayList<>();
        taskList = new ArrayList<>();
        for (int i = 1; i <= 2; i++) {
            @NotNull final ProjectDTO project = new ProjectDTO();
            project.setName("Project");
            project.setDescription("Description");
            project.setUserId(i == 1 ? USER_ID_1 : USER_ID_2);
            projectDtoService.add(project.getUserId(), project);
            projectList.add(project);
        }
        for (int i = 1; i <= INIT_COUNT_TASKS; i++) {
            @NotNull final TaskDTO task = new TaskDTO();
            task.setName("Task_" + i);
            task.setDescription("Description_" + i);
            task.setUserId(USER_ID_1);
            taskDtoService.add(USER_ID_1, task);
            taskList.add(task);
        }
        for (int i = 1; i <= INIT_COUNT_TASKS; i++) {
            @NotNull final TaskDTO task = new TaskDTO();
            task.setName("Task_" + i);
            task.setDescription("Description_" + i);
            task.setUserId(USER_ID_2);
            taskDtoService.add(USER_ID_2, task);
            taskList.add(task);
        }
    }

    @Test
    public void testBindTaskToProjectNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectTaskService.bindTaskToProject(NULLABLE_USER_ID, NULLABLE_PROJECT_ID, NULLABLE_TASK_ID));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectTaskService.bindTaskToProject(EMPTY_USER_ID, NULLABLE_PROJECT_ID, NULLABLE_TASK_ID));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> projectTaskService.bindTaskToProject(USER_ID_1, NULLABLE_PROJECT_ID, NULLABLE_TASK_ID));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> projectTaskService.bindTaskToProject(USER_ID_1, EMPTY_PROJECT_ID, NULLABLE_TASK_ID));
        Assert.assertThrows(TaskIdEmptyException.class, () -> projectTaskService.bindTaskToProject(USER_ID_1, UUID.randomUUID().toString(), NULLABLE_TASK_ID));
        Assert.assertThrows(TaskIdEmptyException.class, () -> projectTaskService.bindTaskToProject(USER_ID_1, UUID.randomUUID().toString(), EMPTY_TASK_ID));
        Assert.assertThrows(ProjectNotFoundException.class, () -> projectTaskService.bindTaskToProject(UUID.randomUUID().toString(), UUID.randomUUID().toString(), UUID.randomUUID().toString()));
        Assert.assertThrows(TaskNotFoundException.class, () -> projectTaskService.bindTaskToProject(USER_ID_1, projectRepository.findOneByIndex(USER_ID_1, 1).getId(), UUID.randomUUID().toString()));
    }

    @Test
    public void testBindTaskToProjectPositive() {
        for (final ProjectDTO project : projectList) {
            Assert.assertEquals(Collections.emptyList(), taskRepository.findAllByProjectId(project.getUserId(), project.getId()));
            for (final TaskDTO task : taskList) {
                if (project.getUserId().equals(task.getUserId()))
                    projectTaskService.bindTaskToProject(project.getUserId(), project.getId(), task.getId());
            }
            List<TaskDTO> tasks = taskRepository.findAll(project.getUserId());
            for (final TaskDTO task : tasks) {
                Assert.assertNotNull(
                        taskRepository.findAllByProjectId(project.getUserId(), project.getId())
                                .stream()
                                .filter(m -> task.getUserId().equals(m.getUserId()))
                                .filter(m -> task.getId().equals(m.getId()))
                                .findFirst()
                                .orElse(null)
                );
            }
        }
    }

    @Test
    public void testUnbindTaskFromProjectNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectTaskService.unbindTaskFromProject(NULLABLE_USER_ID, NULLABLE_PROJECT_ID, NULLABLE_TASK_ID));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectTaskService.unbindTaskFromProject(EMPTY_USER_ID, NULLABLE_PROJECT_ID, NULLABLE_TASK_ID));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> projectTaskService.unbindTaskFromProject(USER_ID_1, NULLABLE_PROJECT_ID, NULLABLE_TASK_ID));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> projectTaskService.unbindTaskFromProject(USER_ID_1, EMPTY_PROJECT_ID, NULLABLE_TASK_ID));
        Assert.assertThrows(TaskIdEmptyException.class, () -> projectTaskService.unbindTaskFromProject(USER_ID_1, UUID.randomUUID().toString(), NULLABLE_TASK_ID));
        Assert.assertThrows(TaskIdEmptyException.class, () -> projectTaskService.unbindTaskFromProject(USER_ID_1, UUID.randomUUID().toString(), EMPTY_TASK_ID));
        Assert.assertThrows(ProjectNotFoundException.class, () -> projectTaskService.unbindTaskFromProject(UUID.randomUUID().toString(), UUID.randomUUID().toString(), UUID.randomUUID().toString()));
        Assert.assertThrows(TaskNotFoundException.class, () -> projectTaskService.unbindTaskFromProject(USER_ID_1, projectRepository.findOneByIndex(USER_ID_1, 1).getId(), UUID.randomUUID().toString()));
    }

    @Test
    public void TestUnbindTaskFromProjectPositive() {
        testBindTaskToProjectPositive();
        for (final ProjectDTO project : projectList) {
            Assert.assertNotEquals(Collections.emptyList(), taskRepository.findAllByProjectId(project.getUserId(), project.getId()));
            for (final TaskDTO task : taskList) {
                if (project.getUserId().equals(task.getUserId()))
                    projectTaskService.unbindTaskFromProject(project.getUserId(), project.getId(), task.getId());
            }
            List<TaskDTO> tasks = taskRepository.findAll(project.getUserId());
            for (final TaskDTO task : tasks) {
                Assert.assertNull(
                        taskRepository.findAllByProjectId(project.getUserId(), project.getId())
                                .stream()
                                .filter(m -> task.getUserId().equals(m.getUserId()))
                                .filter(m -> task.getId().equals(m.getId()))
                                .findFirst()
                                .orElse(null)
                );
            }
        }
    }

    @Test
    public void testRemoveProjectByIdNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectTaskService.removeProjectById(NULLABLE_USER_ID, NULLABLE_PROJECT_ID));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectTaskService.removeProjectById(EMPTY_USER_ID, NULLABLE_PROJECT_ID));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> projectTaskService.removeProjectById(USER_ID_1, NULLABLE_PROJECT_ID));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> projectTaskService.removeProjectById(USER_ID_1, EMPTY_PROJECT_ID));
        Assert.assertThrows(ProjectNotFoundException.class, () -> projectTaskService.removeProjectById(UUID.randomUUID().toString(), UUID.randomUUID().toString()));
    }

    @Test
    public void testRemoveProjectByIdPositive() {
        testBindTaskToProjectPositive();
        for (final ProjectDTO project : projectList) {
            Assert.assertNotNull(taskRepository.findAllByProjectId(project.getUserId(), project.getId()));
            Assert.assertEquals(INIT_COUNT_TASKS, taskRepository.findAll(project.getUserId()).size());
            projectTaskService.removeProjectById(project.getUserId(), project.getId());
            Assert.assertEquals(Collections.emptyList(), taskRepository.findAllByProjectId(project.getUserId(), project.getId()));
            Assert.assertEquals(0, taskRepository.findAll(project.getUserId()).size());
        }
        Assert.assertEquals(0, taskRepository.getSize(USER_ID_1) + taskRepository.getSize(USER_ID_2));
    }

}
