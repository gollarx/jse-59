package ru.t1.shipilov.tm.service.model;

import liquibase.Liquibase;
import liquibase.exception.LiquibaseException;
import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.t1.shipilov.tm.api.service.model.IProjectService;
import ru.t1.shipilov.tm.api.service.model.IProjectTaskService;
import ru.t1.shipilov.tm.api.service.model.ITaskService;
import ru.t1.shipilov.tm.api.service.model.IUserService;
import ru.t1.shipilov.tm.configuration.ServerConfiguration;
import ru.t1.shipilov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.shipilov.tm.exception.entity.field.ProjectIdEmptyException;
import ru.t1.shipilov.tm.exception.entity.field.TaskIdEmptyException;
import ru.t1.shipilov.tm.exception.entity.field.UserIdEmptyException;
import ru.t1.shipilov.tm.marker.UnitCategory;
import ru.t1.shipilov.tm.migration.AbstractSchemeTest;
import ru.t1.shipilov.tm.model.Project;
import ru.t1.shipilov.tm.model.Task;
import ru.t1.shipilov.tm.model.User;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import static ru.t1.shipilov.tm.constant.ProjectTaskConstant.*;

@Category(UnitCategory.class)
public class ProjectTaskServiceTest extends AbstractSchemeTest {

    @NotNull
    private ApplicationContext context = new AnnotationConfigApplicationContext(ServerConfiguration.class);

    @NotNull
    private IProjectService projectService = context.getBean(IProjectService.class);

    @NotNull
    private ITaskService taskService = context.getBean(ITaskService.class);

    @NotNull
    private IProjectTaskService projectTaskService = context.getBean(IProjectTaskService.class);

    @NotNull
    private IUserService userService = context.getBean(IUserService.class);

    private static User USER_1;

    private static User USER_2;

    private static long USER_ID_COUNTER = 0;

    @NotNull
    private List<Project> projectList;

    @NotNull
    private List<Task> taskList;

    @BeforeClass
    public static void changeSchema() throws LiquibaseException {
        final Liquibase liquibase = liquibase("changelog/changelog-master.xml");
        liquibase.dropAll();
        liquibase.update("scheme");
    }

    @Before
    public void init() {
        USER_ID_COUNTER++;
        USER_1 = userService.create("proj_tsk_serv_mod_usr_1_" + USER_ID_COUNTER, "1");
        USER_2 = userService.create("proj_tsk_serv_mod_usr_2_" + USER_ID_COUNTER, "1");
        projectList = new ArrayList<>();
        taskList = new ArrayList<>();
        for (int i = 1; i <= 2; i++) {
            @NotNull final Project project = new Project();
            project.setName("Project");
            project.setDescription("Description");
            project.setUser(i == 1 ? USER_1 : USER_2);
            projectService.add(project.getUser().getId(), project);
            projectList.add(project);
        }
        for (int i = 1; i <= INIT_COUNT_TASKS; i++) {
            @NotNull final Task task = new Task();
            task.setName("Task_" + i);
            task.setDescription("Description_" + i);
            task.setUser(USER_1);
            taskService.add(USER_1.getId(), task);
            taskList.add(task);
        }
        for (int i = 1; i <= INIT_COUNT_TASKS; i++) {
            @NotNull final Task task = new Task();
            task.setName("Task_" + i);
            task.setDescription("Description_" + i);
            task.setUser(USER_2);
            taskService.add(USER_2.getId(), task);
            taskList.add(task);
        }
    }

    @Test
    public void testBindTaskToProjectNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectTaskService.bindTaskToProject(NULLABLE_USER_ID, NULLABLE_PROJECT_ID, NULLABLE_TASK_ID));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectTaskService.bindTaskToProject(EMPTY_USER_ID, NULLABLE_PROJECT_ID, NULLABLE_TASK_ID));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> projectTaskService.bindTaskToProject(USER_1.getId(), NULLABLE_PROJECT_ID, NULLABLE_TASK_ID));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> projectTaskService.bindTaskToProject(USER_1.getId(), EMPTY_PROJECT_ID, NULLABLE_TASK_ID));
        Assert.assertThrows(TaskIdEmptyException.class, () -> projectTaskService.bindTaskToProject(USER_1.getId(), UUID.randomUUID().toString(), NULLABLE_TASK_ID));
        Assert.assertThrows(TaskIdEmptyException.class, () -> projectTaskService.bindTaskToProject(USER_1.getId(), UUID.randomUUID().toString(), EMPTY_TASK_ID));
        Assert.assertThrows(ProjectNotFoundException.class, () -> projectTaskService.bindTaskToProject(UUID.randomUUID().toString(), UUID.randomUUID().toString(), UUID.randomUUID().toString()));
    }

    @Test
    public void testBindTaskToProjectPositive() {
        for (final Project project : projectList) {
            Assert.assertEquals(Collections.emptyList(), taskService.findAllByProjectId(project.getUser().getId(), project.getId()));
            for (final Task task : taskList) {
                if (project.getUser().getId().equals(task.getUser().getId()))
                    projectTaskService.bindTaskToProject(project.getUser().getId(), project.getId(), task.getId());
            }
            List<Task> tasks = taskService.findAll(project.getUser().getId());
            for (final Task task : tasks) {
                Assert.assertNotNull(
                        taskService.findAllByProjectId(project.getUser().getId(), project.getId())
                                .stream()
                                .filter(m -> task.getUser().getId().equals(m.getUser().getId()))
                                .filter(m -> task.getId().equals(m.getId()))
                                .findFirst()
                                .orElse(null)
                );
            }
        }
    }

    @Test
    public void testUnbindTaskFromProjectNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectTaskService.unbindTaskFromProject(NULLABLE_USER_ID, NULLABLE_PROJECT_ID, NULLABLE_TASK_ID));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectTaskService.unbindTaskFromProject(EMPTY_USER_ID, NULLABLE_PROJECT_ID, NULLABLE_TASK_ID));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> projectTaskService.unbindTaskFromProject(USER_1.getId(), NULLABLE_PROJECT_ID, NULLABLE_TASK_ID));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> projectTaskService.unbindTaskFromProject(USER_1.getId(), EMPTY_PROJECT_ID, NULLABLE_TASK_ID));
        Assert.assertThrows(TaskIdEmptyException.class, () -> projectTaskService.unbindTaskFromProject(USER_1.getId(), UUID.randomUUID().toString(), NULLABLE_TASK_ID));
        Assert.assertThrows(TaskIdEmptyException.class, () -> projectTaskService.unbindTaskFromProject(USER_1.getId(), UUID.randomUUID().toString(), EMPTY_TASK_ID));
        Assert.assertThrows(ProjectNotFoundException.class, () -> projectTaskService.unbindTaskFromProject(UUID.randomUUID().toString(), UUID.randomUUID().toString(), UUID.randomUUID().toString()));
    }

    @Test
    public void TestUnbindTaskFromProjectPositive() {
        testBindTaskToProjectPositive();
        for (final Project project : projectList) {
            Assert.assertNotEquals(Collections.emptyList(), taskService.findAllByProjectId(project.getUser().getId(), project.getId()));
            for (final Task task : taskList) {
                if (project.getUser().getId().equals(task.getUser().getId()))
                    projectTaskService.unbindTaskFromProject(project.getUser().getId(), project.getId(), task.getId());
            }
            List<Task> tasks = taskService.findAll(project.getUser().getId());
            for (final Task task : tasks) {
                Assert.assertNull(
                        taskService.findAllByProjectId(project.getUser().getId(), project.getId())
                                .stream()
                                .filter(m -> task.getUser().getId().equals(m.getUser().getId()))
                                .filter(m -> task.getId().equals(m.getId()))
                                .findFirst()
                                .orElse(null)
                );
            }
        }
    }

    @Test
    public void testRemoveProjectByIdNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectTaskService.removeProjectById(NULLABLE_USER_ID, NULLABLE_PROJECT_ID));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectTaskService.removeProjectById(EMPTY_USER_ID, NULLABLE_PROJECT_ID));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> projectTaskService.removeProjectById(USER_1.getId(), NULLABLE_PROJECT_ID));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> projectTaskService.removeProjectById(USER_1.getId(), EMPTY_PROJECT_ID));
        Assert.assertThrows(ProjectNotFoundException.class, () -> projectTaskService.removeProjectById(UUID.randomUUID().toString(), UUID.randomUUID().toString()));
    }

    @Test
    public void testRemoveProjectByIdPositive() {
        testBindTaskToProjectPositive();
        for (final Project project : projectList) {
            Assert.assertNotNull(taskService.findAllByProjectId(project.getUser().getId(), project.getId()));
            Assert.assertEquals(INIT_COUNT_TASKS, taskService.findAll(project.getUser().getId()).size());
            projectTaskService.removeProjectById(project.getUser().getId(), project.getId());
            Assert.assertEquals(Collections.emptyList(), taskService.findAllByProjectId(project.getUser().getId(), project.getId()));
            Assert.assertEquals(0, taskService.findAll(project.getUser().getId()).size());
        }
        Assert.assertEquals(0, taskService.findAll(USER_1.getId()).size() + taskService.findAll(USER_2.getId()).size());
    }

}
